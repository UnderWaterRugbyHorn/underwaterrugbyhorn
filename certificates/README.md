# Certificates and Passwords

This is the location to place all certificates and passwords. These files should not be uploaded to gitlab. The three folders correspond to the three sevices running in docker. Each servies mounts its respective folder.


## Setup

Run the following script to setup the nessecary password and certificate files.
```bash
./generate.sh
```
The script makes use of `htpasswd` and `mosquitto_passwd` provided by:
- Fedora: `httpd-tools`, `mosquitto`
- Ubuntu: `apache2-utils`, `mosquitto`


## TODO

- create password files for the python backend and the javascript, so that they do not need to be hardcoded.


## Passwords

### Webpage (nginx)
- This password is needed to log into the Game Controler webpage.
    - folder: `webserver`
    - filename: `passwd_webpage`
- This is the password used to log into the mqtt service.
    - folder: `webserver`
    - filename: `passwd_mqtt`
    - username: `GameControllerUI`
    - It is curretnly hardcoded into the javscript code.

### Game controller (python)
- These are the passwords needed by python to log into the mqtt service.
    - folder: `gamecontroller`
    - filename: `passwd_mqtt`
    - username: `GameController`
    - currently the paswords are hardcoded into the python code.

### Mqtt server (mosquitto)
- These are the users and passwords that are allowd to write mqtt messages. These passwords need to match to the ones above.
    - folder: `mqtt`
    - filename: `passwd`
    - usernames: `GameControllerUI`, `GameController`


## Certificates
Currently nothing is enctypted, so only passwords are needed. This is unsecure, as the passwords can be intercepted.

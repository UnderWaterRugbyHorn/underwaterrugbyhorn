/*
##########################
# Listening and Updating
##########################
*/

mqtt_config["topics"] = ["tournament/gameday",
                         "tournament/lineup",
                         "tournament/roster",
                         "tournament/settings",
                         "state/real_time",
                         "state/game_time",
                         "state/game_state",
                         "state/game_config",
                         "state/protocol",
                         "state/linked_timers",
                         "command/answer"];

var new_protocol_event = {};
var is_new_event = true;
var event_descriptions = {
    'goal': 'Goal',
    'timeout': 'Timeout',
    'penaltythrow': 'Penalty Throw',
    'freethrow': 'Free Throw',
    'warning': 'Warning',
    'teamwarning': 'Team Warning',
    'penaltytime': 'Penalty Time',
    'doublepenaltytime': 'Double Penalty Time',
    'matchpenalty': 'Match Penalty',
    'teamball': 'Team Ball',
    'playerexchange': 'Player Exchange',
    'refereeball': 'Referee Ball',
    'comment': 'Comment'};

// for these events ask for a team color
var need_teamcolor = ['penaltytime', 'doublepenaltytime', 'matchpenalty',
                      'freethrow', 'warning', 'teamwarning',
                      'timeout', 'playerexchange', 'goal',
                      'teamball', 'penaltythrow'];

// these events are linked to the gametimer
var with_linked_timer = ['penaltytime', 'doublepenaltytime', 'matchpenalty'];

// these events are for a team, eg. goal for blue. All other events are against,
// eg. penalty throw against blue.
var event_for_team;
if (tournamentSettings.german_events) {
    event_for_team =  ['goal', 'timeout', 'teamball', 'playerexchange'];
} else {
    event_for_team =  ['goal', 'timeout', 'teamball', 'playerexchange', 'freethrow', 'penaltythrow'];
}


var touchEvent;

var changes = {'players_in': [], 'players_out': []};

$(document).ready(function() {
    MQTTconnect();
    // First we check if you support touch, otherwise it's click:
    // Then we bind via that event.
    // This way we only bind one event, instead of the two for touch and click.
    touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
    add_event_listeners();
    $('#commentModal').on('hidden.bs.modal', save_desc);
    $('#penaltyModal').on('hidden.bs.modal', save_desc);
    $('#lineupModal').on('hidden.bs.modal', saveLineUpPlayer);
});

mqtt_messages.onGameState = function (topic, payload) {
    let old_game_state = game_state
    MQTTdefaultMessages.prototype.onGameState.call(this, topic, payload);
    if (game_state.score_blue != $('#score_blue').html()) $('#score_blue').html(game_state.score_blue);
    if (game_state.score_white != $('#score_white').html()) $('#score_white').html(game_state.score_white);
    let statetext = "H"+game_state.n_halftime;
    if (statetext !=  $('#HalfTime').html()) $('#HalfTime').html(statetext);
    if (game_state.is_timeout) {
        statetext = "Timeout";
    } else if (game_state.is_penaltythrow) {
        statetext = "Penalty Throw";
    } else if (game_state.is_halftime_break) {
        statetext = "Halftime Break";
    } else if (game_state.is_match_over) {
        statetext = "Ended";
        // setTimeout(game_ended_modal, 120000); This was for modal to pop up after a game has ended but this was annoying in penalty shootouts
    } else {
        statetext = "";
    }
    if (statetext != $('#game_state_text').html()) $('#game_state_text').html(statetext);
    if (game_state.is_gametime_running){
        $('#GameStatus').removeClass("fa-stop-circle GameStatusRed").addClass("fa-play-circle GameStatusGreen");
    }
    else{
        $('#GameStatus').removeClass("fa-play-circle GameStatusGreen").addClass("fa-stop-circle GameStatusRed");
    }
    let extra_html = "Add Extra Halftime";
    if (game_state.is_extra_halftime) extra_html = "Remove Extra Halftime";
    if (extra_html != $("#ToggleExtraHalftime").html()) $("#ToggleExtraHalftime").html(extra_html);
    update_linked_timers();
    if (old_game_state && game_state) {
        if (game_state.is_halftime_break) {
            // do nothing here
        } else if (game_state.is_match_over) {
            // open end of game modal, if old_game_state was not match over
        } else if (!game_state.is_gametime_running) {
            if (old_game_state.is_gametime_running) {
                // a normal game stop: open the select event modal
                if (!($("#commentModal").hasClass('show') || $("#teamcolorModal").hasClass('show') || $("#selectEventModal").hasClass('show'))) {
                    let last_game_id = protocol[protocol.length-1].id;
                    open_select_event_modal(last_game_id);
                }
            }
        }
    }
};

mqtt_messages.onGameTime = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameTime.call(this, topic, payload);
    if (game_time != $('#game_time').html()) {
        $('#game_time').html(game_time);
    }
    update_linked_timers();
};

mqtt_messages.onGameConfig = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameConfig.call(this, topic, payload);
    $('#game_length').val(game_config.game_length);
    $('#extra_game_length').val(game_config.extra_game_length);
    $('#penaltytime_length').val(game_config.penaltytime_length);
    $('#penaltythrow_length').val(game_config.penaltythrow_length);
    $('#timeout_length').val(game_config.timeout_length);
    $('#matchpenalty_length').val(game_config.matchpenalty_length);
    $('#halftime_break_length').val(game_config.halftime_break_length);
    $('#n_halftime_total').val(game_config.n_halftime_total);
    if (game_config.continuous_time) {
        $('#continuous_time').prop('checked', 'checked');
        $('#stopped_time').removeProp('checked');
    }
    else {
        $('#continuous_time').removeProp('checked');
        $('#stopped_time').prop('checked', 'checked');
    }
    $('#input_teamname_blue').val(game_config.teamname_blue);
    $('#teamname_blue').html(game_config.teamname_blue);
    $('#input_teamname_white').val(game_config.teamname_white);
    $('#teamname_white').html(game_config.teamname_white);
    $("#SelectedGame").val(game_config.game_id);
};

mqtt_messages.onProtocol = function (topic, payload) {
    MQTTdefaultMessages.prototype.onProtocol.call(this, topic, payload);
    update_protocol();
};

mqtt_messages.onGameday = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameday.call(this, topic, payload);
    update_game_options();
};

mqtt_messages.onRoster = function (topic, payload) {
    MQTTdefaultMessages.prototype.onRoster.call(this, topic, payload);
    update_game_options();
};

mqtt_messages.onTournamentSettings = function (topic, payload) {
    MQTTdefaultMessages.prototype.onTournamentSettings.call(this, topic, payload);
    if (tournamentSettings.german_events) {
        event_for_team =  ['goal', 'timeout', 'teamball', 'playerexchange'];
    } else {
        event_for_team =  ['goal', 'timeout', 'teamball', 'playerexchange', 'freethrow', 'penaltythrow'];
    }
};

var update_game_options = function () {
    let inner_html = '<option value="-1" selected>No Game Selected</option>\n';
    if (gameday && roster) {
        for(let i = 0; i < gameday.length; i++) {
            let teamblue = gameday[i]['blue'];
            let teamwhite = gameday[i]['white'];
            if (teamblue in roster) {
                teamblue = roster[teamblue].name;
            }
            if (teamwhite in roster) {
                teamwhite = roster[teamwhite].name;
            }
            inner_html += '<option value="' + gameday[i]['id'] + '">';
            inner_html += '[id=' + gameday[i]['id'] + ']  "';
            inner_html += teamblue + '"  versus  "' + teamwhite + '" (';
            inner_html += plan_formatter(gameday[i]['plan'], '', '', '') + ')';
            inner_html += '</option>\n';
        }
    }
    $('#SelectedGame').html(inner_html);
    if (game_config) {
        $('#SelectedGame').val(game_config.game_id);
    }
};

function update_protocol () {
    if (!protocol) return;
    let inner_html = "";
    for (let i = protocol.length - 1; i >= 0; i--) {
        let game_stop = protocol[i];
        inner_html +=
            `<div class="row align-items-center border-top mt-2 pt-1 border-secondary prot-gs-id${game_stop.id} prot-gamestop">` +
            `<div class="col-4 lead font-weight-bold prot-time">${game_stop.gametime} (${game_stop.halftime})</div>` +
            `<div class="col-8 text-right">` +
            `<button class="btn btn-secondary" type="button" onclick="open_select_event_modal('${game_stop.id}')">Add Event</button></div></div>`;
        for (let j = game_stop.events.length - 1; j >= 0; j--) {
            let event = game_stop.events[j];
            let color_class = '';
            if (event.teamcolor == 'blue') {
                color_class = 'Blue';
            } else if (event.teamcolor == 'white') {
                color_class = 'White';
            }
            inner_html +=
                `<div class="row align-items-center ${color_class} m-1 prot-gs-id${game_stop.id} prot-e-id${event.id}">`;
            if (with_linked_timer.includes(event.event_type)) {
                inner_html += `<div class="col-2 prot-extra text-center linkedtimer-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'timeout') {
                inner_html += `<div class="col-2 prot-extra text-center timeout-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'halftimebreak') {
                inner_html += `<div class="col-2 prot-extra text-center halftimebreak-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'penaltythrow') {
                inner_html += `<div class="col-2 prot-extra text-center penaltythrow-extra" data-id="${event.timer_id}"></div>`;
            } else {
                inner_html +=
                    `<div class="col-2 prot-extra text-center">${event.extra}</div>`;
            }
            inner_html +=
                `<div class="col-1 prot-number">${event.player_nr}</div>
<div class="col-7 prot-text">
  <span class="font-weight-bold lead mr-3 prot-ddisabled="disabled"esc">${event.desc}</span>
  <span class="prot-comment">${event.comment}</span>
</div>
<div class="col-2">
  <button type="button" class="btn btn-secondary py-1 px-3 m-1" onclick="delete_event(${game_stop.id},${event.id})">
    <i class="fa fa-trash uwr-icons"></i></button>
  <button type="button" class="btn btn-secondary py-1 px-3 m-1" onclick="edit_event(${game_stop.id}, ${event.id})">
    <i class="fa fa-pencil uwr-icons"></i></button>
</div></div>`;
        }
    }
    $("#protocol-list").html(inner_html);
    update_linked_timers();
}

function update_linked_timers () {
    if (linked_timers == null) return;
    if (!protocol) return;
    $('.linkedtimer-extra').each(function(i, obj) {
        if (obj.dataset.id in linked_timers) {
            let fa_icon;
            let set_active;
            let inner_html;
            if (linked_timers[obj.dataset.id].active) {
                fa_icon = 'fa-play';
                set_active = false;
                inner_html = `<span>${linked_timers[obj.dataset.id].countdown}</span> `;
            } else {
                fa_icon = 'fa-pause';
                set_active = true;
                inner_html = `<span class='text-muted' style="text-decoration: line-through;">${linked_timers[obj.dataset.id].countdown}</span> `;
            }
            inner_html += `<button type="button" class="btn btn-secondary" onclick="set_linkedtimer_active(${obj.dataset.id}, ${set_active})">
    <i class="fa ${fa_icon} uwr-icons"></i>
</button>`;
            if (obj.innerHTML != inner_html) obj.innerHTML = inner_html;
        } else {
            if (obj.innerHTM != 'Ended') obj.innerHTML = 'Ended';
        }
    });
    if (!game_state) return;
    $('.timeout-extra').each(function(i, obj) {
        if (game_state.is_timeout && obj.dataset.id == timeout_time.id) {
            if (obj.innerHTML != timeout_time.countdown) {
                obj.innerHTML = timeout_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
    $('.halftimebreak-extra').each(function(i, obj) {
        if (game_state.is_halftime_break && obj.dataset.id == halftimebreak_time.id) {
            if (obj.innerHTML != halftimebreak_time.countdown) {
                obj.innerHTML = halftimebreak_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
    $('.penaltythrow-extra').each(function(i, obj) {
        if (game_state.is_penaltythrow && obj.dataset.id == penaltythrow_time.id) {
            if (obj.innerHTML != penaltythrow_time.countdown) {
                obj.innerHTML = penaltythrow_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
}

function set_linkedtimer_active(timer_id, active) {
    let myObj = {'timer_id': timer_id,
                 'active': active};
    mqtt_messages.send("command/set_linkedtimer_active", JSON.stringify(myObj), false);
}

function get_event_desc(event_type, team_color) {
    let new_desc = event_descriptions[event_type];
    if (need_teamcolor.includes(event_type)) {
        if (event_for_team.includes(event_type)) {
            new_desc = new_desc + ' for ';
        } else {
            new_desc = new_desc + ' against ';
        }
        if (game_config) {
            if (team_color == 'blue') {
                new_desc = new_desc + game_config.teamname_blue;
            } else if (team_color == 'white') {
                new_desc = new_desc + game_config.teamname_white;
            }
        } else {
            new_desc = new_desc + team_color;
        }
    }
    return new_desc;
}

function open_select_event_modal(game_stop_id) {
    new_protocol_event = {'game_stop_id': game_stop_id,
                          'event_type': '',
                          'teamcolor': '',
                          'player_nr': '',
                          'comment': ''};
    if (protocol[game_stop_id]) {
        $("#selectEventModal-header").html(`What happend at ${protocol[game_stop_id].gametime} (${protocol[game_stop_id].halftime})?`)
    } else {
        $("#selectEventModal-header").html("What happend?")
    }
    $("#commentModal").modal("hide");
    $("#teamcolorModal").modal("hide");
    $("#selectEventModal").modal("show");
}

function open_teamcolor_modal(event_type) {
    $('#endOfGameModal').modal('hide');
    $("#selectEventModal").modal("hide");
    $("#commentModal").modal("hide");
    $('#teamcolorModal-blue').html(
        `<button type="button" class="btn btn-secondary p-4" onclick="add_event('${event_type}', 'blue')">${get_event_desc(event_type, "blue")}</button>`);
    $('#teamcolorModal-white').html(
        `<button type="button" class="btn btn-secondary p-4" onclick="add_event('${event_type}', 'white')">${get_event_desc(event_type, "white")}</button>`);
    $('#teamcolorModal').on('hidden.bs.modal', );
    $('#teamcolorModal').off('hidden.bs.modal');
    $("#teamcolorModal").modal("show");
}

function add_event(event_type, team_color) {
    new_protocol_event.event_type = event_type;
    new_protocol_event.desc = event_descriptions[event_type];
    new_protocol_event.teamcolor = team_color;
    $("#selectEventModal").modal("hide");
    $("#teamcolorModal").modal("hide");
    let team;
    let game;
    for(let i = 0; i < gameday.length; i++) {
        if (gameday[i].id == game_config.game_id) {
            team = gameday[i][team_color];
            game = gameday[i];
        }
    }
    if (new_protocol_event.event_type == 'timeout') {
        mqtt_messages.send("command/add_event", JSON.stringify(new_protocol_event), false);
    }
    else if (new_protocol_event.event_type == 'playerexchange') {
        if (tournamentSettings.use_lineup) {
            is_new_event = true;
            setLineupModal(game, team_color, team);
        }
        else {
            is_new_event = true;
            open_comment_modal();
        }
    }
    else {
        is_new_event = true;
        open_comment_modal();
    }
}

function setLineupModal (game, color, team){
    let id = game_config.game_id;
    $('#lineupModal').off('hidden.bs.modal');
    $('#lineupModal').on('hidden.bs.modal', saveLineUpPlayer);
    changes.players_in = [];
    changes.players_out = [];
    $('#form-playersin').val(JSON.stringify(changes.players_in).replace(/["\[\]]/g,""));
    $('#form-playersout').val(JSON.stringify(changes.players_out).replace(/["\[\]]/g,""));
    if (typeof lineup['starting_lineup'] == 'undefined') {
        alert("No starting lineup was set for this team.");
        return;
    }
    if (typeof lineup['starting_lineup'][id] == 'undefined') {
        alert("No starting lineup was set for this team.");
        return;
    }
    if (lineup['starting_lineup'][id] == {}) {
        alert("No starting lineup was set for this team.");
        return;
    }
    if (typeof lineup['starting_lineup'][id][team] == 'undefined') {
        alert("No starting lineup was set for this team.");
        return;
    }
    if (lineup['starting_lineup'][id][team] == {}) {
        alert("No starting lineup was set for this team.");
        return;
    }

    $('#form-gameid').val(id);
    $('#form-color').val(color);
    $('#form-team').data('team_id', team);
    if (team in roster) {
        $('#form-team').val(roster[team]["name"]);
    }
    else {
        alert("Team not found in the roster");
        return;
    }

    $('#form-plantime').val(moment.utc(game['plan']).local().format("DD.MM.YYYY HH:mm"));

    // check, if there is already a line up, or if it is possible to set one up
    if (typeof lineup[id] == "undefined"){
        alert("No lineup was set for this team.");
        return;
    }
    if (typeof lineup[id][team] == "undefined"){
        alert("No lineup was set for this team.");
        return;
    }
    $('#form-captain').val(lineup[id][team].captain);
    $('#form-leader').val(lineup[id][team].leader);

    // add all players from the roster to the lineup
    let playerAdded = false;
    for (var i in roster[team]["players"]) {
        if (!(i in lineup[id][team].member)) {
            lineup[id][team].member[i] = "out";
            playerAdded = true;
        }
    }
    if (playerAdded) {
        alert("A player was added to the roster since this starting lineup was set up. Please check!");
    }

    var tablerows = "";
    $('#form-captain').empty();
    let player = 0;
    let water = 0;
    let out = 0;
    let sub = 0;
    for (let i in lineup[id][team].member) {
        // check if a player in the lineup is still in the roster
        let player_name = 'Player ' + i;
        let player_passnumber = '';
        if (i in roster[team]["players"]) {
            player_name = roster[team]["players"][i]["name"];
            player_passnumber =  roster[team]["players"][i]["passnumber"];
        }
        player++;

        tablerows += '<div class="row"><div class="col-2 text-right"><p>';
        tablerows += i;
        tablerows += '</p></div><div class="col-6"><p>';
        tablerows += player_name;
        tablerows += " (";
        tablerows += player_passnumber;
        tablerows += ")";
        tablerows += '</p></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="water" ';
        if (lineup[id][team].member[i] == "water") {
            tablerows += "checked ";
            water++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','water')";
        if (lineup[id][team].member[i] == "out") {
            tablerows += " disabled='disabled'";
        }
        tablerows += ' /></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="sub" ';
        if (lineup[id][team].member[i] == "sub" || lineup[id][team].member[i] == "exchanged") {
            tablerows += "checked ";
            sub++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','sub')";
        if (lineup[id][team].member[i] == "out") {
            tablerows += " disabled='disabled'";
        }
        tablerows += ' /></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="out" ';
        if (lineup[id][team].member[i] == "out"){
            tablerows += "checked ";
            out++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','out') disabled='disabled'";
        tablerows += ' /></div></div>';
        if (i == lineup[id][team].captain) {
            $('#form-captain').append($('<option>', {
                value: i,
                selected: "",
                text: i + " " + player_name
            }));
        } else {
            $('#form-captain').append($('<option>', {
                value: i,
                text: i + " " + player_name
            }));
        }

    }
    $('#player_list').html(tablerows);
    $('#sum-player').html(player);
    $('#sum-water').html(water);
    $('#sum-sub').html(sub);
    $('#sum-out').html(out);

    // open the modal after successfull setup
    // ensures teamcolorModal is properly closed before opening the next modal. This ensures enabling of scrolling in lineupModal
    $('#teamcolorModal').on('hidden.bs.modal', function () {
        $('#lineupModal').modal('show');
        $('#teamcolorModal').off('hidden.bs.modal');
    });
}

function uptdateLineUpPlayer(id, team, player, state){
    let exchangeToSub = false;
    if (lineup[id][team].member[player] == 'exchanged') {
        lineup[id][team].member[player] = 'sub';
        exchangeToSub = true;
    }
    let water = parseInt($('#sum-water').html());
    let sub = parseInt($('#sum-sub').html());
    let out = parseInt($('#sum-out').html());
    if (lineup[id][team].member[player] != state) {
        switch(lineup[id][team].member[player]) {
        case "water":
            water--;
            if (changes.players_in.includes(player)) {
                changes.players_in.splice(changes.players_in.indexOf(player), 1);
            }
            else if (!changes.players_out.includes(player)) {
                changes.players_out.push(player);
            }
            break;
        case "out":
            alert("This should not be possible")
            out--;
            break;
        case "sub":
            sub--;
            if (changes.players_out.includes(player)) {
                changes.players_out.splice(changes.players_out.indexOf(player), 1);
            }
            else if (!changes.players_in.includes(player)) {
                changes.players_in.push(player);
            }
            break;
        default:
            break;
        }
        lineup[id][team].member[player] = state;
        switch(state) {
        case "water":
            water++;
            break;
        case "out":
            alert("This should not be possible")
            out++;
            break;
        case "sub":
            sub++;
            lineup[id][team].member[player] = 'exchanged';
            break;
        default:
            break;
        }
        
        $('#sum-water').html(water);
        $('#sum-sub').html(sub);
        $('#sum-out').html(out);
        $('#form-playersin').val(JSON.stringify(changes.players_in).replace(/["\[\]]/g,""));
        $('#form-playersout').val(JSON.stringify(changes.players_out).replace(/["\[\]]/g,""));
    }
    else if(exchangeToSub) {
        lineup[id][team].member[player] = 'exchanged';
    }
};

function saveLineUpPlayer(){
    let cur_id   = $('#form-gameid').val();
    let cur_team = $('#form-team').data('team_id');
    let cur_color = $('#form-color').val()
    let water = parseInt($('#sum-water').html());
    let sub = parseInt($('#sum-sub').html());
    if (changes.players_in.length != changes.players_out.length) {
        alert("ERROR! Not the same number of players entering and leaving the water. Please start again");
        for (let i = 0; i < changes.players_in.length; i++) {
            player_nr = parseInt(changes.players_in[i]);
            lineup[cur_id][cur_team].member[player_nr] = 'sub';
        }
        changes.players_in = [];
        for (let i = 0; i < changes.players_out.length; i++) {
            player_nr = parseInt(changes.players_out[i]);
            lineup[cur_id][cur_team].member[player_nr] = 'water';
        }
        changes.players_out = [];
        let game;
        for(let i = 0; i < gameday.length; i++) {
            if (gameday[i].id == game_config.game_id) {
                game = gameday[i];
            }
        }
        setLineupModal(game, cur_color, cur_team);
        $('#lineupModal').modal('show');
        return;
    }
    if (changes.players_in.length > 3) {
        alert("ERROR! Only 3 exchanges allowed per team");
        $('#lineupModal').modal('show');
        return;
    }
    if (water > 12) {
        alert("ERROR! You have more then 12 players in the water");
        $('#lineupModal').modal('show');
        return;
    }
    if (sub > 3) {
        alert("ERROR! You have more then 3 substitutes");
        $('#lineupModal').modal('show');
        return;
    }
    let comment = "";
    let oldCaptain = "";
    if (lineup[cur_id][cur_team].captain != $('#form-captain').val()) {
        comment = " ; new captain: " + $('#form-captain').val();
        oldCaptain = lineup[cur_id][cur_team].captain;
    }
    lineup[cur_id][cur_team].captain = $('#form-captain').val();
    if (Object.keys(roster[cur_team]['players']).length > 0 && lineup[cur_id][cur_team].member[lineup[cur_id][cur_team].captain] != "water") {
        alert("ERROR! The captain must be in the water. Please choose a new captain.");
        $('#lineupModal').modal('show');
        return;
    }
    lineup[cur_id][cur_team].leader = $('#form-leader').val();
    
    mqtt_messages.send("tournament/lineup", JSON.stringify(lineup));
    save_exchange(comment, oldCaptain);
};

function edit_event(game_stop_id, event_id) {
    for (let i = 0; i < protocol[game_stop_id]['events'].length; i++) {
        if (protocol[game_stop_id]['events'][i]['id'] == event_id) {
            new_protocol_event = protocol[game_stop_id]['events'][i];
            break;
        }
    }
    new_protocol_event.game_stop_id = game_stop_id;
    new_protocol_event.event_id = event_id;
    if (new_protocol_event.event_type == 'playerexchange' && tournamentSettings.use_lineup) {
        alert("Player exchanges can not be editted. Please delete it and create a new exchange instead!")
        return;
    }
    is_new_event = false;
    open_comment_modal();
}

function open_comment_modal() {
    let teamblue = "Blue";
    let teamwhite = "White";
    for(let i = 0; i < gameday.length; i++) {
        if (gameday[i].id == game_config.game_id) {
            teamblue = gameday[i]['blue'];
            teamwhite = gameday[i]['white'];
        }
    }
    $('#commentModal-EventDesc').html(get_event_desc(new_protocol_event.event_type, new_protocol_event.teamcolor));
    $('#commentModal-Comment').val(new_protocol_event.comment);
    let team_not_selected;
    let team_selected;
    if (new_protocol_event.teamcolor == 'blue') {
        team_not_selected = teamwhite;
        team_selected = teamblue;
        $('#commentModal-header').addClass("Blue").removeClass("White");
    } else if (new_protocol_event.teamcolor == 'white') {
        team_not_selected = teamblue;
        team_selected = teamwhite;
        $('#commentModal-header').addClass("White").removeClass("Blue");
    } else {
        $('#commentModal-header').removeClass("Blue").removeClass("White");
    }
    
    if (new_protocol_event.event_type != "penaltythrow") {
        $('#form-number-text').val(new_protocol_event.player_nr)
        $('#form-number').empty();
        $('#form-number').append($('<option>', {
            value: "",
            text: ""
        }));
        if (typeof lineup[game_config.game_id] == 'undefined') {
            $('#commentModal').modal('show');
            return;
        }
        if (tournamentSettings.use_lineup) {
            if (new_protocol_event.teamcolor != '' && typeof lineup[game_config.game_id][team_selected] != 'undefined') {
                for (var i in lineup[game_config.game_id][team_selected].member) {
                    if (lineup[game_config.game_id][team_selected].member[i] == 'water' || (!is_new_event && (lineup[game_config.game_id][team_selected].member[i] == 'exchanged'))) {
                        player_name = "Player " + i;
                        if (i in roster[team_selected]["players"]) {
                            player_name = roster[team_selected]["players"][i]["name"];
                        }
                        if (new_protocol_event.player_nr == i){
                            $('#form-number').append($('<option>', {
                                selected: true,
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                        else {
                            $('#form-number').append($('<option>', {
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                    }
                }
            }                                                     
        }
        $('#freethrow-label').empty();
        if (new_protocol_event.event_type == 'freethrow' && !event_for_team.includes('freethrow')) {
            $('#freethrow-label').text('This is the player who caused the freethrow, not the one executing it from the other team.');
        }
        $('#commentModal').modal('show');
    }
    else {
        if (event_for_team.includes(new_protocol_event.event_type)) {
            tmp  = team_selected;
            team_selected = team_not_selected;
            team_not_selected = tmp;
        }
        $('#form-number-attacking').empty();
        $('#form-number-defending').empty();
        $('#form-number-attacking').append($('<option>', {
            value: "",
            text: ""
        }));
        $('#form-number-defending').append($('<option>', {
            value: "",
            text: ""
        }));
        let comment = new_protocol_event.comment.split(" ; ");
        let temp_comment = ''
        let previous_attacker = -1;
        let previous_defender = -1;
        for (var i = 0; i < comment.length; i++) {
            if (comment[i].includes('attacking')) {
                previous_attacker = parseInt(comment[i].split()[0]);
            }
            else if (comment[i].includes('defending')) {
                previous_defender = parseInt(comment[i].split()[0]);
            }
            else {
                temp_comment += comment[i]
            }
        }
        $('#penaltyModal-Comment').val(temp_comment)
        if (previous_attacker == -1) {
            $('#form-number-attacking-text').val('')
        }
        else {
            $('#form-number-attacking-text').val(previous_attacker)
        }
        if (previous_defender == -1) {
            $('#form-number-defending-text').val('')
        }
        else {
            $('#form-number-defending-text').val(previous_defender)
        }

        if (typeof lineup[game_config.game_id] == 'undefined') {
            $('#penaltyModal').modal('show');
            return;
        }
        if (tournamentSettings.use_lineup) {
            if (new_protocol_event.teamcolor != '' && typeof lineup[game_config.game_id][team_not_selected] != 'undefined') {
                for (var i in lineup[game_config.game_id][team_not_selected].member) {
                    if (lineup[game_config.game_id][team_not_selected].member[i] == 'water' || (!is_new_event && (lineup[game_config.game_id][team_not_selected].member[i] == 'exchanged'))) {
                        player_name = "Player " + i;
                        if (i in roster[team_not_selected]["players"]) {
                            player_name = roster[team_not_selected]["players"][i]["name"];
                        }
                        if (previous_attacker == i){
                            $('#form-number-attacking').append($('<option>', {
                                selected: true,
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                        else {
                            $('#form-number-attacking').append($('<option>', {
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                    }
                }
            }
            if (new_protocol_event.teamcolor != '' && typeof lineup[game_config.game_id][team_selected] != 'undefined') {
                for (var i in lineup[game_config.game_id][team_selected].member) {
                    if (lineup[game_config.game_id][team_selected].member[i] == 'water' || (!is_new_event && (lineup[game_config.game_id][team_selected].member[i] == 'exchanged'))) {
                        player_name = "Player " + i;
                        if (i in roster[team_selected]["players"]) {
                            player_name = roster[team_selected]["players"][i]["name"];
                        }
                        if (previous_defender == i){
                            $('#form-number-defending').append($('<option>', {
                                selected: true,
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                        else {
                            $('#form-number-defending').append($('<option>', {
                                value: i,
                                text: i + " " + player_name
                            }));
                        }
                    }
                }
            }
        }
        $('#penaltyModal').modal('show');
    }

}

function save_desc() {
    let temp_comment = $('#commentModal-Comment').val();
    let is_penaltythrow = (new_protocol_event.event_type == 'penaltythrow');
    let temp_player_nr;
    let temp_nr_attacking;
    let temp_nr_defending;
    if (is_penaltythrow) {
        if (tournamentSettings.use_lineup) {
            temp_nr_attacking = $('#form-number-attacking').val();
            temp_nr_defending = $('#form-number-defending').val();
        }
        else {
            temp_nr_attacking = $('#form-number-attacking-text').val();
            temp_nr_defending = $('#form-number-defending-text').val();
        }
        temp_comment = $('#penaltyModal-Comment').val();
    }
    else {
        if (tournamentSettings.use_lineup) {
            temp_player_nr = $('#form-number').val();
        }
        else {
            temp_player_nr = $('#form-number-text').val();
        }
    }
    if (is_new_event) {
        if (is_penaltythrow) {
            new_protocol_event.comment = temp_comment;
            if (!(temp_nr_attacking == null || temp_nr_attacking == "")) {
                new_protocol_event.comment = new_protocol_event.comment + " ; " + temp_nr_attacking + " attacking";
            }
            if (!(temp_nr_defending == null || temp_nr_defending == "")) {
                new_protocol_event.comment = new_protocol_event.comment + " ; " + temp_nr_defending + " defending";
            }
            $("#penaltyModal").modal("hide");
        }
        else {
            new_protocol_event.player_nr = temp_player_nr;
            new_protocol_event.comment = temp_comment;
            $("#commentModal").modal("hide");
        }
        mqtt_messages.send("command/add_event", JSON.stringify(new_protocol_event), false);
    } else {
        if (is_penaltythrow) {
            temp_comment = temp_comment.split(' ; ')[0];
            if (!(temp_nr_attacking == null || temp_nr_attacking == "")) {
                temp_comment = temp_comment + " ; " + temp_nr_attacking + " attacking";
            }
            if (!(temp_nr_defending == null || temp_nr_defending == "")) {
                temp_comment = temp_comment + " ; " + temp_nr_defending + " defending";
            }
            if (temp_comment != new_protocol_event.comment) {
                new_protocol_event.comment = temp_comment;
                mqtt_messages.send("command/edit_event", JSON.stringify(new_protocol_event), false);
            }
            $("#penaltyModal").modal("hide");
        }
        else {
            if (temp_player_nr != new_protocol_event.player_nr || temp_comment != new_protocol_event.comment) {
                new_protocol_event.player_nr = temp_player_nr;
                new_protocol_event.comment = temp_comment;
                mqtt_messages.send("command/edit_event", JSON.stringify(new_protocol_event), false);
            }
            $("#commentModal").modal("hide");
        }
    }
}

function save_exchange(comment, oldCaptain) {
    players_in = changes.players_in;
    players_out = changes.players_out;
    for (let i = 0; i < players_in.length; i++) {
        let temp_player_nr = players_out[i];
        let temp_comment = players_in[i] + ' new in the water';
        let captain_comment = "";
        if (players_out[i] == oldCaptain) {
            captain_comment = comment;
        };
        if (is_new_event) {
            new_protocol_event.player_nr = temp_player_nr;
            new_protocol_event.comment = temp_comment + captain_comment;
            mqtt_messages.send("command/add_event", JSON.stringify(new_protocol_event), false);
        } else {
            if (temp_player_nr != new_protocol_event.player_nr || temp_comment != new_protocol_event.comment) {
                new_protocol_event.player_nr = temp_player_nr;
                new_protocol_event.comment = temp_comment + captain_comment;
                mqtt_messages.send("command/edit_event", JSON.stringify(new_protocol_event), false);
            }
        }
    }
    changes.players_in = [];
    changes.players_out = [];
}

function delete_event(game_stop_id, event_id) {
    if (confirm("Are you sure you want to delete this event?")) {
        if (tournamentSettings.use_lineup) {
            for (let i = 0; i < protocol[game_stop_id]['events'].length; i++) {
                if (protocol[game_stop_id]['events'][i]['id'] == event_id) {
                    new_protocol_event = protocol[game_stop_id]['events'][i];
                    break;
                }
            }
            new_protocol_event.game_stop_id = game_stop_id;
            new_protocol_event.event_id = event_id;
            is_new_event = false;
            if (new_protocol_event.event_type == 'playerexchange') {
                player_was_out = new_protocol_event.player_nr;
                player_was_in = parseInt(new_protocol_event.comment.split(" ")[0]);
                team_color = new_protocol_event.teamcolor;
                for(let i = 0; i < gameday.length; i++) {
                    if (gameday[i].id == game_config.game_id) {
                        team_id = gameday[i][team_color];
                    }
                }
                if (lineup["starting_lineup"][game_config.game_id][team_id] != null && lineup["starting_lineup"][game_config.game_id][team_id].member[player_was_in] != null) {
                    if (lineup["starting_lineup"][game_config.game_id][team_id].member[player_was_in] == 'water'){
                        lineup[game_config.game_id][team_id].member[player_was_in] = 'exchanged';
                    }
                    else {
                        lineup[game_config.game_id][team_id].member[player_was_in] = 'sub';
                    }
                }
                if (lineup[game_config.game_id][team_id].member[player_was_out] != null) {
                    lineup[game_config.game_id][team_id].member[player_was_out] = 'water';
                }
                if (player_was_in == lineup[game_config.game_id][team_id].captain) {
                    alert("Attention! You deleted an exchange where the current captain came into the water. This means the captain is now no longer in the water. Please check the lineup and choose a new captain!");
                }
                mqtt_messages.send("tournament/lineup", JSON.stringify(lineup))
            }
        }
        let myObj = {'game_stop_id': game_stop_id,
                     'event_id': event_id};
        mqtt_messages.send("command/delete_event", JSON.stringify(myObj), false);
    }
}

function game_ended_modal() {
    if ($('#game_state_text').html() == "Ended") {
        $('#endOfGameModal').modal('show');
    }
}

function add_penaltythrow() {
    let last_game_id = protocol[protocol.length-1].id;
    new_protocol_event = {'game_stop_id': last_game_id,
                          'event_type': '',
                          'teamcolor': '',
                          'player_nr': '',
                          'comment': ''};
    open_teamcolor_modal('penaltythrow');
}

function add_extra_halftime() {
    let myObj;
    if (game_state && game_state.is_extra_halftime){
        myObj= {"turn_on":"false"};
    }
    else{
        myObj= {"turn_on":"true"};
    }
    mqtt_messages.send("command/set_extra_halftime", JSON.stringify(myObj), false);
    $('#endOfGameModal').modal('hide');
}

function select_next_game() {
    let gameID = game_config.game_id;
    let selectNext = false;
    for(let i = 0; i < gameday.length; i++) {
        if (selectNext) {
            gameID = gameday[i].id;
            teamblue = gameday[i]['blue'];
            teamwhite = gameday[i]['white'];
            selectNext = false;
            break;
        }
        if (gameday[i].id == gameID) {
            selectNext = true;
        }
    }
    let myobj = {"new_id":gameID};
    mqtt_messages.send("command/select_game", JSON.stringify(myobj), false);
    $('#endOfGameModal').modal("hide");
    if (tournamentSettings.use_lineup) {
        if (JSON.stringify(lineup) == '{}' || typeof lineup['starting_lineup'][gameID] == 'undefined' || JSON.stringify(lineup['starting_lineup'][gameID]) == "{}") {
            my_alert("No starting lineups set. Please check lineup for BOTH teams on Gameday!");
        }
        else if ((typeof lineup['starting_lineup'][gameID][teamblue] == 'undefined') || (JSON.stringify(lineup['starting_lineup'][gameID][teamblue]) == "{}")) {
            my_alert("No starting lineup set for " + roster[teamblue]['name'] + "!");
        }
        else if (typeof lineup['starting_lineup'][gameID][teamwhite] == 'undefined' || JSON.stringify(lineup['starting_lineup'][gameID][teamwhite]) == "{}") {
            my_alert("No starting lineup set for " + roster[teamwhite]['name'] + "!");
        }
    }
}

function cancelExchange() {
    let cur_id   = $('#form-gameid').val();
    let cur_team = $('#form-team').data('team_id');
    for (let i = 0; i < changes.players_in.length; i++) {
        player_nr = parseInt(changes.players_in[i]);
        lineup[cur_id][cur_team].member[player_nr] = 'sub';
    }
    changes.players_in = [];
    for (let i = 0; i < changes.players_out.length; i++) {
        player_nr = parseInt(changes.players_out[i]);
        lineup[cur_id][cur_team].member[player_nr] = 'water';
    }
    changes.players_out = [];
    $('#lineupModal').off('hidden.bs.modal');
    $('#lineupModal').on('hidden.bs.modal', );
    $('#lineupModal').modal('hide');
}

/*
########################
# Events and Publishing
########################
*/

add_event_listeners = function () {
    /*$('#Horn').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/horn_request", '""', false);
    });
    */

    $('#StartClock').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/start_game", '""', false);
    });

    $('#StopClock').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/stop_game", '""', false);
    });

    $('#ForceStart').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/force_start_game", '""', false);
    });

    $('#ResetGame').on(touchEvent, function(e) {
        if (confirm("Are you sure you want to reset this game? This will reset both halftimes!")) {
            if (!(typeof lineup['starting_lineup'] == 'undefined') && !(typeof lineup['starting_lineup'][game_config.game_id] == 'undefined') && lineup['starting_lineup'][game_config.game_id] != {}) {
                lineup[game_config.game_id] = lineup['starting_lineup'][game_config.game_id];
                mqtt_messages.send("tournament/lineup", JSON.stringify(lineup));
            }
            e.preventDefault();
            mqtt_messages.send("command/reset_game", '""', false);
        }
    });

    $('#SetConfig').on(touchEvent, function(e) {
        e.preventDefault();
        var myObj= {"game_length": $('#game_length').val(),
                    "extra_game_length": $('#extra_game_length').val(),
                    "penaltytime_length": $('#penaltytime_length').val(),
                    "penaltythrow_length": $('#penaltythrow_length').val(),
                    "timeout_length": $('#timeout_length').val(),
                    "matchpenalty_length": $('#matchpenalty_length').val(),
                    "halftime_break_length": $('#halftime_break_length').val(),
                    "n_halftime_total": $('#n_halftime_total').val(),
                    "continuous_time": $('input[name="continuous_time"]:checked').val()};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#settings-menu').collapse("hide");
    });

    $('#SetDefault').on(touchEvent, function(e) {
        e.preventDefault();
        var myObj= {"game_length": 900,
                    "extra_game_length": 900,
                    "penaltytime_length": 120,
                    "penaltythrow_length": 45,
                    "timeout_length": 60,
                    "matchpenalty_length": 300,
                    "halftime_break_length": 300,
                    "n_halftime_total": 2,
                    "continuous_time": false};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#settings-menu').collapse("hide");
    });

    $('#SetTeamNames').on(touchEvent, function(e) {
        e.preventDefault();
        let myObj = {"teamname_blue": $('#input_teamname_blue').val(),
                     "teamname_white": $('#input_teamname_white').val()};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#teamname-menu').collapse("hide");
    });

    $('#SetGameID').on(touchEvent, function(e) {
        e.preventDefault();
        let gameID = $('#SelectedGame').val();
        let myobj = {"new_id":gameID};
        mqtt_messages.send("command/select_game", JSON.stringify(myobj), false);
        $('#teamname-menu').collapse("hide");
        for(let i = 0; i < gameday.length; i++) {
            if (gameday[i].id == gameID) {
                teamblue = gameday[i]['blue'];
                teamwhite = gameday[i]['white'];
            }
        }
        if (tournamentSettings.use_lineup) {
            if (JSON.stringify(lineup) == '{}' || typeof lineup['starting_lineup'][gameID] == 'undefined' || JSON.stringify(lineup['starting_lineup'][gameID]) == "{}") {
                my_alert("No starting lineups set. Please check lineup for BOTH teams on Gameday!");
            }
            else if ((typeof lineup['starting_lineup'][gameID][teamblue] == 'undefined') || (JSON.stringify(lineup['starting_lineup'][gameID][teamblue]) == "{}")) {
                my_alert("No starting lineup set for " + roster[teamblue]['name'] + "!");
            }
            else if (typeof lineup['starting_lineup'][gameID][teamwhite] == 'undefined' || JSON.stringify(lineup['starting_lineup'][gameID][teamwhite]) == "{}") {
                my_alert("No starting lineup set for " + roster[teamwhite]['name'] + "!");
            }
        }
    });

    $('#ToggleExtraHalftime').on(touchEvent, function(e) {
        e.preventDefault();
        let myObj;
        if (game_state && game_state.is_extra_halftime){
            myObj= {"turn_on":"false"};
        }
        else{
            myObj= {"turn_on":"true"};
        }
        mqtt_messages.send("command/set_extra_halftime", JSON.stringify(myObj), false);
    });
};

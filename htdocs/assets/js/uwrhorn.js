/*
####################
# Global variables #
####################
*/
var MQTTclient;
var mqtt_is_connected = false;
var game_state = null;
var game_config = null;
var roster =  null;
var lineup = null;
var gameday = null;
var protocol = null;
var linked_timers = null;
var ended_linked_timers = {};
var real_time = null;
var game_time = null;
var timeout_time = null;
var halftimebreak_time = null;
var penaltythrow_time = null;
var delay = null;
var reconnectTimeout = 2000;
var $gamedaytable = null;
var gamedaytable_on_editing = false;
var tournamentSettings = {
    name: "Tournament Name",
    use_lineup: false,
    show_penalty_time: false,
    german_events: false
};

/*
####################
# Global functions #
####################
*/
var my_alert = function (text) {
    text = '<div role="alert" class="alert alert-warning beautiful" ><div><button type="button" class="close" data-dismiss="alert"><i class="fa fa-window-close-o" aria-hidden="true"></i></button><strong>Warning! </strong>' + text;
    text += '</div></div>';
    $('#alert-box').html(text);
};

var download_file = function (fileUrl, fileName) {
  let a = document.createElement("a");
  a.href = fileUrl;
  a.setAttribute("download", fileName);
  a.click();
}


var get_game_state_text = function () {
    if (game_state == null) return "";
    if (game_state.is_timeout) return "TO";
    if (game_state.is_penaltythrow) return "P";
    if (game_state.is_halftime_break) return "B";
    if (game_state.is_match_over) return "Ended";
    return "H"+game_state.n_halftime;
};

var get_time_text = function () {
    if (game_time == null) return "";
    if (game_state == null) return game_time;
    if (game_state.is_timeout && timeout_time != null) return timeout_time.countdown;
    if (game_state.is_penaltythrow && penaltythrow_time != null) return penaltythrow_time.countdown;
    if (game_state.is_halftime_break && halftimebreak_time != null) return halftimebreak_time.countdown;
    return game_time;
};

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ( (charCode > 31 && charCode < 48) || charCode > 57) {
        return false;
    }
    return true;
};

/*
###################
# MQTT connection #
###################
*/
var MQTTconnect = function () {
	MQTTclient = new Paho.Client(
		mqtt_config.host,
		mqtt_config.port,
        mqtt_config.path,
		"web_" + parseInt(Math.random() * 1e15, 10)
	);
    var options = {
        timeout: 3,
        useSSL: mqtt_config.useTLS,
        cleanSession: mqtt_config.cleansession,
        onSuccess: mqtt_messages.onConnect,
        onFailure: mqtt_messages.onFailure,
    };
    if (mqtt_config.username) {
        options.userName = mqtt_config.username;
        options.password = mqtt_config.password;
    }

    MQTTclient.onConnectionLost = mqtt_messages.onConnectionLost;
    MQTTclient.onMessageArrived = mqtt_messages.onMessageArrived;
    // console.log("Host=" + mqtt_config.host +
    //             " port=" + mqtt_config.port +
    //             " path=" + mqtt_config.path +
    //             " TLS=" + mqtt_config.useTLS +
    //             " username=" + mqtt_config.username +
    //             " password=" + mqtt_config.password);
    MQTTclient.connect(options);
};

var MQTTdefaultMessages = function () {
};

MQTTdefaultMessages.prototype.onConnect = function () {
    mqtt_is_connected = true;
    $('#footer').html('Connected to ' +
                      mqtt_config.host + ':' +
                      mqtt_config.port +
                      mqtt_config.path);
    // Connection succeeded; subscribe to our topics
    mqtt_config.topics.forEach(function (item, index, array) {
        MQTTclient.subscribe(item, {qos: 1});
    });
};

MQTTdefaultMessages.prototype.onConnectionLost = function (response) {
    mqtt_is_connected = false;
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#footer').html("connection lost: " + response.errorMessage + ". Reconnecting");
};

MQTTdefaultMessages.prototype.onFailure = function (response) {
    mqtt_is_connected = false;
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#footer').html("failure: " + response.errorMessage + ". Reconnecting");
};
    
MQTTdefaultMessages.prototype.onMessageArrived = function (message) {
    let topic = message.destinationName;
    let payload = message.payloadString;
    try {
        payload = JSON.parse(payload);
    }
    catch (err) { 
        $('#error_msg').html(err.message);
    }

    switch(topic) {
    case "state/real_time":
        mqtt_messages.onRealTime(topic, payload);
        break;
    case "state/game_time":
        mqtt_messages.onGameTime(topic, payload);
        break;
    case "state/game_state":
        mqtt_messages.onGameState(topic, payload);
        break;
    case "state/game_config":
        mqtt_messages.onGameConfig(topic, payload);
        break;
    case "state/protocol":
        mqtt_messages.onProtocol(topic, payload);
        break;
    case "tournament/delay":
        mqtt_messages.onDelay(topic, payload);
        break;
    case "tournament/gameday":  
        mqtt_messages.onGameday(topic, payload);
        break;
    case "tournament/roster":
        mqtt_messages.onRoster(topic, payload);
        break;
    case "tournament/settings":
        mqtt_messages.onTournamentSettings(topic, payload);
        break;
    case "tournament/lineup":
        mqtt_messages.onLineup(topic, payload);
        break;
    case "command/answer":
        mqtt_messages.onAnswer(topic, payload);
    case "command/print_answer":
        mqtt_messages.onPrintAnswer(topic, payload);
    default:
        $('#' + topic.split("/")[1]).html(payload);
    }
};
    
MQTTdefaultMessages.prototype.onGameday = function(topic, payload) {
    gameday = payload;
};

MQTTdefaultMessages.prototype.onGameState = function(topic, payload) {
    game_state = payload;
};

MQTTdefaultMessages.prototype.onProtocol = function(topic, payload) {
    protocol = payload;
};

MQTTdefaultMessages.prototype.onDelay = function(topic, payload) {
    delay = payload;
    if ($gamedaytable &&
        gamedaytable_on_editing == false &&
        gameday) {
        for (let index = 0; index < gameday.length; index++) {
            $gamedaytable.bootstrapTable('updateCell', {'index': index, 'field': "", 'value': "", 'reinit': true});
        }
    }
};

MQTTdefaultMessages.prototype.onRealTime = function(topic, payload) {
    $('#real-time').html(moment.utc(payload).local().format("HH:mm:ss"));
};

MQTTdefaultMessages.prototype.onGameTime = function(topic, payload) {
    game_time = payload.game;
    timeout_time = payload.timeout;
    halftimebreak_time = payload.halftimebreak;
    penaltythrow_time = payload.penaltythrow;
    linked_timers = payload.linked_timers;
    if ($gamedaytable &&
        gamedaytable_on_editing == false &&
        game_config &&
        game_config.game_id > 0) {
        let index;
        for (index = 0; index < gameday.length; index++) {
            if (game_config.game_id == gameday[index].id) {
                break;
            }
        }
        $gamedaytable.bootstrapTable('updateCell', {'index': index, 'field': "time", 'value': "", 'reinit': true});
    }
};

MQTTdefaultMessages.prototype.onRoster = function(topic, payload) {
    roster = payload;
};

MQTTdefaultMessages.prototype.onLineup = function(topic, payload) {
    lineup = payload;
};

MQTTdefaultMessages.prototype.onTournamentSettings = function(topic, payload) {
    tournamentSettings = payload;
    $('#tournamentName').text(tournamentSettings.name);
    if (tournamentSettings.use_lineup) {
        $('.lineup-mode-only').show()
        $('.non-lineup-mode-only').hide()
    }
    else {
        $('.lineup-mode-only').hide()
        $('.non-lineup-mode-only').show()
    }
};

MQTTdefaultMessages.prototype.onGameConfig = function(topic, payload) {
    game_config = payload;
};

MQTTdefaultMessages.prototype.onAnswer = function(topic, payload) {
    my_alert(payload);
};

MQTTdefaultMessages.prototype.onPrintAnswer = function(topic, payload) {
    if (payload.mqtt_client_id == MQTTclient.clientId) {
        download_file(payload.url, payload.filename)
    }
};

MQTTdefaultMessages.prototype.send = function(topic, payload, retained=true) {
    if (mqtt_is_connected) {
        MQTTclient.send(topic, payload, 1, retained);
    }
    else {
        my_alert("The MQTT broker is not connected, jet. Wait until it is connected, before you start to make changes.");
    }
};

var mqtt_messages = new MQTTdefaultMessages();

/*
############################
# Gameday table formatting #
############################
*/
function plan_formatter(value, row, index, field) {
    /*
      value: value of the field
      row: object of row with {col_name: vlaue, ...}
      index: row index
      field: the column name, s.th. row.field = value
    */
    let dt = moment.utc(value).local();
    if (dt.isValid()) {
        return dt.format("DD.MM.YYYY HH:mm");
    }
    else {
        return value;
    }
}

function time_formatter(value, row, index, field) {
    let out = "";
    if (value != "") {
        // if something is written in the value use it
        out = value;
    } 
    else if (game_config && row.id == game_config.game_id) {
        // Check if the game is in a not running state.
        let state_text = get_game_state_text();
        let time_text = get_time_text();
        if (game_state.is_match_over) {
            out = state_text;
        } else {
            out = state_text + ' - ' + time_text;
        }
    }
    else if (delay && row.id in delay) {
        out = "Exp. " + moment.utc(row.plan).local().add(delay[row.id], 's').format("HH:mm");
    }
    else {
        out = "Exp. " + moment.utc(row.plan).local().format("HH:mm");
    }
    return out;
}


function teamname_formatter(value, row, index, field) {
    let out = "";
    if (roster && value in roster) {
        out = roster[value].name;
    }
    else {
        out = value;
    }
    return out;
}


function category_formatter(value, row, index) {
    let out = "";
    
    if (gameday[index].gender_category == "f") {
        out = '<div class="text-center"><b>F</b></div>';
    }
    else {
        out = '<div class="text-center"><b>M</b></div>';
    }
    return out;
}

function gameType_formatter(value, row, index) {
    let out = "";
    out = gameday[index].gameType;
    return out;
}

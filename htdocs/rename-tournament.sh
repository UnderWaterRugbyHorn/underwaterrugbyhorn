#!/bin/bash
# Set name of turnament shown on website
# Execute like this
# 	bash rename-turnament.sh "Berliner Bär"

git stash
find ./ -name "*.html" -type f -exec sed -i "s/DUC Hamburg/$1/g" {} \;
git status

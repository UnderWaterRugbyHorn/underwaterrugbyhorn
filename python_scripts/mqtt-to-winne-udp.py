#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pprint import pprint
import socket
import time
import json
import traceback
import subprocess
import threading
import paho.mqtt.client as mqtt
import argparse

# this is only needed to put the path name together
import os
import logging

Logger = logging.getLogger(__name__)

IS_EMULATOR = False
DELAY_TIME = 0.0

if IS_EMULATOR:
    import numpy as np


parser = argparse.ArgumentParser(
    description=
    (
        "Listen to MQTT messages from UWR horn and send them via UDP to Winne's"
        " livestream"
    )
)
parser.add_argument('--udp-ip', default="255.255.255.255", help="IP address to send to")
parser.add_argument('--udp-port', default=9889, help="Port to send to")
parser.add_argument('--mqtt-ip', default="uwr.local", help="IP address to listen to")
parser.add_argument('--mqtt-port', default=1883, help="Port to listen to")
parser.add_argument('--no-game-state', action="store_true", help="Don't send game state UDP (DA... string)")
args = parser.parse_args()

class StreamMqttClient():

    def __init__(self):
        self.client = mqtt.Client(clean_session=True)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.connected = False

        self.teamname_blue = ''
        self.teamname_white = ''
        self.gametime = ''
        self.script_lock = threading.Lock()
        self.timefile_lock = threading.Lock()
        self.delay_lock = threading.Lock()

        self.last_time_string = ''
        self.old_command_list = []

        self._game_time = " 0:00"
        self._status = "E"
        self._score_blue = "00"
        self._score_white = "00"
        self._game_counter = "00"
        self._halftime_idx = " "
        self._team_color = " "
        self._blue_timers = ["  :00", "  :00", "  :00"]
        self._white_timers = ["  :00", "  :00", "  :00"]
        self._horn_state = "0"
        self._udp_sring_format = (
            "DA{game_time}{status}{score_blue}-{score_white}{game_counter}{halftime_idx}"
            "{team_color}{blue_timer_0}{blue_timer_1}{blue_timer_2}"
            "{white_timer_0}{white_timer_1}{white_timer_2}{horn_state}"
        )

        self.active_game_id = None
        self.active_game = None
        self.gameday = None
        self.roster = None
        self.lineup = None
        self.event_list = None
        self.team_int_ids = {}
        self.team_counter = 1
        # time: 3-7
        # status: 8 (R: running, T: time out, P: penalty shot, E: end of game)
        # score_blue: 9-10
        # 11: -
        # score_white: 12-13
        # game_counter: 14-15
        # halftime_idx: 16 (a: first, b: second)
        # team_color: 17 (b: blue, w: white, " ": none)
        # blue_penalty_timeout: 18-29
        # white_penalty_timeout: 30-41
        # blue/white_penalty: 4 digits (minutes:seconds) per timer (3 per team)

        #self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        #self.udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #self.udp_socket.bind(('255.255.255.255', 9889))

    @staticmethod
    def remove_leading_zeros(time: str):
        if time[0] == '0':
            # replace first 0 with space
            time = f" {time[1:]}"
            if time[1] == '0':
                # replace first and second 0 with space
                time = f"  {time[2:]}"
        return time

    @staticmethod
    def _test_game_state_attribute(attribute, length, expected_chars={}):
        """
        Test if the attribute is a string and has the expected length and constrain
        positions in attribute to certain chars. E.g. ``expected_chars={2: ':'}`` tests
        for character 2 to be ':' in attribute.
        """
        if type(attribute) != str:
            raise TypeError(
                f"Game state attribute must be a string, got {attribute} of type"
                f" {type(attribute)}"
            )

        if len(attribute) != length:
            raise ValueError(
                f"Expected attribute with {length} characters, got attribute"
                f" '{attribute}'"
            )

        for position, chars in expected_chars.items():
            if attribute[position] not in chars:
                raise ValueError(
                    f"Expected on of '{chars}' at position {position}, got attribute"
                    f" '{attribute}'"
                )

    @property
    def game_time(self):
        return self._game_time

    @game_time.setter
    def game_time(self, new_time: str):
        self._test_game_state_attribute(new_time, 5, expected_chars={2: ':'})
        self._game_time = new_time

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, new_status: str):
        self._test_game_state_attribute(
            new_status, 1, expected_chars={0: ['R', 'T', 'P', 'E', 'H']}
        )
        self._status = new_status

    @property
    def score_blue(self):
        return self._score_blue

    @score_blue.setter
    def score_blue(self, new_score: str):
        self._test_game_state_attribute(new_score, 2)
        self._score_blue = new_score

    @property
    def score_white(self):
        return self._score_white

    @score_white.setter
    def score_white(self, new_score: str):
        self._test_game_state_attribute(new_score, 2)
        self._score_white = new_score

    @property
    def game_counter(self):
        return self._game_counter

    @game_counter.setter
    def game_counter(self, new_game_counter: str):
        self._test_game_state_attribute(new_game_counter, 2)
        self._game_counter = new_game_counter

    @property
    def halftime_idx(self):
        return self._halftime_idx

    @halftime_idx.setter
    def halftime_idx(self, new_halftime_idx):
        self._test_game_state_attribute(new_halftime_idx, 1, expected_chars={0: ['a', 'b', 'c']})
        self._halftime_idx = new_halftime_idx

    @property
    def team_color(self):
        return self._team_color

    @team_color.setter
    def team_color(self, new_team_color):
        self._test_game_state_attribute(new_team_color, 1, expected_chars={0: ['b', 'w', ' ']})
        self._team_color = new_team_color

    @property
    def horn_state(self):
        return self._horn_state

    @horn_state.setter
    def horn_state(self, new_horn_state):
        self._test_game_state_attribute(new_horn_state, 1, expected_chars={0: ['H', '0']})
        self._horn_state = new_horn_state

    def _update_timer(self, team: str, timer_idx: int, new_time: str):
        self._test_game_state_attribute(new_time, 5, expected_chars={2: ':'})
        assert team in ['blue', 'white'], team
        assert timer_idx in range(3), timer_idx
        getattr(self, f"_{team}_timers")[timer_idx] = new_time

    def _get_udp_string(self):
        return self._udp_sring_format.format(
            game_time=self.game_time,
            status=self.status,
            score_blue=self.score_blue,
            score_white=self.score_white,
            game_counter=self.game_counter,
            halftime_idx=self.halftime_idx,
            team_color=self.team_color,
            white_timer_0=self._white_timers[0],
            white_timer_1=self._white_timers[1],
            white_timer_2=self._white_timers[2],
            blue_timer_0=self._blue_timers[0],
            blue_timer_1=self._blue_timers[1],
            blue_timer_2=self._blue_timers[2],
            horn_state=self.horn_state,
        )

    def __enter__(self):
        self.client.loop_start()
        print(f"Trying to connect to MQTT broker at {args.mqtt_ip}:{args.mqtt_port}")
        print("...")
        self.client.connect_async(args.mqtt_ip, args.mqtt_port, 60)
        return self

    def __exit__(self, type, value, traceback):
        self.client.loop_stop()
        self.client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print("Connection successful")
        else:
            print("Connected with result code " + str(rc))
        self.connected = True
        self.client.subscribe("state/#", 1)
        self.client.subscribe("tournament/#", 1)
        self.client.subscribe("command/#", 1)

    def on_message(self, *args, **kwargs):
        t = threading.Timer(DELAY_TIME, self._on_message,
                            args, kwargs)
        t.start()

    def _on_message(self, client, userdata, msg):
        with self.delay_lock:
            #print("New message: topic=" + str(msg.topic) + " payload=" +
            #      str(msg.payload))
            if str(msg.topic) == 'state/real_time':
                return
            print()
            print("New message: topic=" + str(msg.topic))
            payload = json.loads(msg.payload)
            pprint(payload)
            update_game_state = False
            update_lineup = False
            update_gameday = False
            update_events = False
            try:
                if str(msg.topic) == 'command/horn':
                    state = str(msg.payload, encoding='utf-8')[1:-1]
                    if state == "horn_on":
                        self.horn_state = "H"
                    elif state == "horn_off":
                        self.horn_state = "0"
                    else:
                        raise ValueError(f"Unknown horn state: {state}")
                if str(msg.topic) == 'state/game_config':
                    game_config = json.loads(msg.payload)
                    if game_config['game_id'] == -1:
                        self.game_counter = '00'
                    else:
                        self.game_counter = f"{game_config['game_id']:02d}"
                    self.active_game_id = game_config["game_id"]
                    #if self.gameday is not None:
                    #    for game in self.gameday:
                    #        if game['id'] == game_config["game_id"]:
                    #            self.active_game = game
                    update_game_state = True
                    update_lineup = True

                elif str(msg.topic) == 'state/game_state':
                    previous_status = self.status
                    game_state = json.loads(msg.payload)
                    self.score_blue = f"{game_state['score_blue']:02d}"
                    self.score_white = f"{game_state['score_white']:02d}"
                    if game_state['n_halftime'] == 1:
                        self.halftime_idx = 'a'
                    elif game_state['n_halftime'] == 2:
                        self.halftime_idx = 'b'
                    elif game_state['n_halftime'] == 3:
                        self.halftime_idx = 'c'
                    else:
                        raise ValueError(
                            f"Unexpected halftime index {game_state['n_halftime']}"
                        )

                    if game_state['is_penaltythrow']:
                        self.status = 'P'
                    elif game_state['is_timeout']:
                        self.status = 'T'
                    elif game_state['is_halftime_break']:
                        self.status = 'H'
                    elif game_state['is_match_over']:
                        self.status = 'E'
                    elif game_state['has_started']:
                        self.status = 'R'

                    # Reset team_color after penaltythrow / timeout is over
                    if (previous_status == 'T' and self.status != 'T') or \
                            (previous_status == 'P' and self.status != 'P'):
                        self.team_color = ' '

                    update_game_state = True

                elif str(msg.topic) == 'state/game_time':
                    # Get game time
                    game_time_dict = json.loads(msg.payload)
                    if self.status == 'R':
                        # Half time timer
                        game_time = game_time_dict['game'].strip()
                        game_time = self.remove_leading_zeros(game_time)
                        self.game_time = game_time
                    elif self.status == 'T':
                        # Timeout timer
                        timeout_time = game_time_dict['timeout']['countdown'].strip()
                        timeout_time = self.remove_leading_zeros(timeout_time)
                        self.game_time = timeout_time
                    elif self.status == 'P':
                        # Penalty timer
                        penalty_time = game_time_dict['penaltythrow']['countdown'].strip()
                        penalty_time = self.remove_leading_zeros(penalty_time)
                        self.game_time = penalty_time
                    elif self.status == 'H':
                        # Half time break timer
                        halftime_break_time = game_time_dict['halftimebreak']['countdown'].strip()
                        halftime_break_time = self.remove_leading_zeros(halftime_break_time)
                        self.game_time = halftime_break_time

                    # Get penalty timers
                    blue_timers = []
                    white_timers = []
                    for _, timer_dict in sorted(game_time_dict['linked_timers'].items(), key=lambda t: int(t[0])):
                        if timer_dict['active']:
                            countdown = timer_dict['countdown'].strip()
                            countdown = self.remove_leading_zeros(countdown)
                            teamcolor = timer_dict['teamcolor'].strip()
                            if teamcolor == 'blue':
                                blue_timers.append(countdown)
                            elif teamcolor == 'white':
                                white_timers.append(countdown)
                            else:
                                raise ValueError(
                                    f"Unexpected team '{teamcolor}'"
                                )
                    # We can only handle 3 timers per team, always show the newest 3
                    # (dictionary above is sorted by key and keys are the timer ids
                    # which are always increasing)
                    # TODO: Log warning when there are more than 3 timers
                    if len(blue_timers) >= 3:
                        self._blue_timers = blue_timers[-3:]
                    else:
                        self._blue_timers[:len(blue_timers)] = blue_timers
                        self._blue_timers[len(blue_timers):] = ["  :00"] * (3 - len(blue_timers))

                    if len(white_timers) >= 3:
                        self._white_timers = white_timers[-3:]
                    else:
                        self._white_timers[:len(white_timers)] = white_timers
                        self._white_timers[len(white_timers):] = ["  :00"] * (3 - len(white_timers))
                    update_game_state = True

                elif str(msg.topic) == 'state/protocol':
                    self.event_list = json.loads(msg.payload)
                    # Check if last event had a penaltythrow or timeout and save the
                    # team color (as long as a penalty / timeout is active, the last
                    # game stop time will have that event)
                    for event in self.event_list[-1]['events']:
                        # There can be only one penaltythrow per game stop
                        if event['event_type'] in ['penaltythrow', 'timeout']:
                            teamcolor = event['teamcolor']
                            if event['teamcolor'] == 'blue':
                                self.team_color = 'b'
                            elif event['teamcolor'] == 'white':
                                self.team_color = 'w'
                            else:
                                self.team_color = ' '
                            break
                        # no timeout / penaltythrow in last game stop
                        self.team_color = ' '

                    update_events = True

                # Tournament
                elif str(msg.topic) == 'tournament/lineup':
                    self.lineup = json.loads(msg.payload)
                    update_lineup = True

                elif str(msg.topic) == 'tournament/roster':
                    self.roster = json.loads(msg.payload)
                    for team_id in self.roster:
                        if team_id not in self.team_int_ids:
                            self.team_int_ids[team_id] = self.team_counter
                            self.team_counter += 1

                    update_lineup = True
                    if self.gameday is not None:
                        update_gameday = True

                elif str(msg.topic) == 'tournament/gameday':
                    self.gameday = json.loads(msg.payload)
                    if self.roster is not None:
                        update_lineup = True
                        update_gameday = True

                else:
                    return

            except Exception as e:
                msg = ('The message caused an error:\n'
                       'topic = ' + str(msg.topic) + '\n'
                       'payload = ' + str(msg.payload) + '\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)
                return
            if update_game_state:
                try:
                    threading.Thread(target=self.update_game_state).start()
                except Exception as e:
                    msg = ('Error calling the script:\n'
                           'error msg = ' + str(e) + '\n' +
                           traceback.format_exc())
                    print(msg)

            if update_lineup:
                try:
                    threading.Thread(target=self.update_lineup).start()
                except Exception as e:
                    msg = ('Error calling the script:\n'
                           'error msg = ' + str(e) + '\n' +
                           traceback.format_exc())
                    print(msg)

            if update_gameday:
                try:
                    threading.Thread(target=self.update_gameday).start()
                except Exception as e:
                    msg = ('Error calling the script:\n'
                           'error msg = ' + str(e) + '\n' +
                           traceback.format_exc())
                    print(msg)

            if update_events:
                try:
                    threading.Thread(target=self.update_events).start()
                except Exception as e:
                    msg = ('Error calling the script:\n'
                           'error msg = ' + str(e) + '\n' +
                           traceback.format_exc())
                    print(msg)

    def update_game_state(self):

        if args.no_game_state:
            return

        with self.script_lock:
            try:
                if not self.connected:
                    return
                    #game_state_string = f"Connecting to MQTT broker at {args.mqtt_ip}:{args.mqtt_port}..."
                    game_state_string = "Not yet connected to MQTT broker..."
                    # Make it 48 chars long, so it shows in UDP receiver
                    game_state_string += " " * (48 - len(game_state_string))
                else:
                    game_state_string = self._get_udp_string()
                print(r"Sending UDP message: " + "\n" + game_state_string)
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                    sock.sendto(bytes(game_state_string, "utf-8"), (args.udp_ip, args.udp_port))

            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

    def update_lineup(self):

        # We need to get tournament/roster, tournament/gameday and lineup first
        if self.gameday is None or self.lineup is None or self.roster is None:
            return

        # If no game is active, send -1
        active_game = self.active_game_id
        if active_game is None:
            active_game = -1

        lineup_lines = [
            # Header lines
            f'lineups,{self.active_game_id}',
            'lineup,game_id,blue_team_id,white_team_id,blue_team_int_id,white_team_int_id,blue_team_name,white_team_name,blue_team_gender,white_team_gender,blue_team_nation,white_team_nation,time,score_blue,score_white,game_type,game_gender_category'
        ]

        for game in self.gameday:
            blue_team = game["blue"]
            white_team = game["white"]
            # In update_gameday, I used this (in case we want to show info when team
            # deleted)
            #
            #   blue_name = ""
            #   # If team is deleted, ID is still present, but name is gone
            #   if blue_id and game['blue'] in self.roster:
            #       blue_name = self.roster[game['blue']]['name']
            #
            if blue_team not in self.roster or white_team not in self.roster:
                continue
            blue_team_roster = self.roster[blue_team]
            blue_team_name = blue_team_roster["name"]
            blue_team_players = blue_team_roster["players"]
            blue_team_int_id = self.team_int_ids[blue_team]
            blue_team_gender = blue_team_roster["gender_category"]
            blue_team_nation = blue_team_roster["nation"]
            white_team_roster = self.roster[white_team]
            white_team_name = white_team_roster["name"]
            white_team_players = white_team_roster["players"]
            white_team_int_id = self.team_int_ids[white_team]
            white_team_gender = white_team_roster["gender_category"]
            white_team_nation = white_team_roster["nation"]
            plan_mqtt = game['plan']
            # fix time difference 1 hour
            date_str, time_str = plan_mqtt.split("T")
            time_str_wo_Z = time_str[:-1]
            hour_str, min_str, sec_str = time_str_wo_Z.split(":")
            new_hour_str = str(int(hour_str) + 1)
            new_time_str = ":".join([new_hour_str, min_str, sec_str]) + "Z"
            plan = "T".join([date_str, new_time_str])
            if game['score']:
                scores = game['score'].split(':')
                #score_blue = f"{int(scores[0].strip()):02d}"
                #score_white = f"{int(scores[1].strip()):02d}"
                score_blue = scores[0].strip()
                score_white = scores[1].strip()
            else:
                score_blue = ""
                score_white = ""
            game_type = game["gameType"]
            game_gender_category = game["gender_category"]
            lineup_lines.append(
                f'lineup,{game["id"]},{blue_team},{white_team},{blue_team_int_id},{white_team_int_id},{blue_team_name},{white_team_name},{blue_team_gender},{white_team_gender},{blue_team_nation},{white_team_nation},{plan},{score_blue},{score_white},{game_type},{game_gender_category}'
            )

            # Toggle this to switch between sending the lineup only for current game or
            # for all games (for large tournamens the latter becomes very big and might
            # slow down clock information send via UDP)
            send_all_games_lineup = False
            if send_all_games_lineup or game["id"] == active_game:
                lineup_lines.append(
                    'teamcolor,player_number,player_name,player_status'
                )

                team = {
                    'blue': blue_team,
                    'white': white_team
                }

                team_players = {
                    'blue': blue_team_players,
                    'white': white_team_players
                }

                # If lineup not set in gameday lineup menu, the csv will have only headers, no
                # data

                if str(game["id"]) in self.lineup:
                    lineup_dict = self.lineup[str(game["id"])]
                    starting_lineup_dict = None
                    if "starting_lineup" in self.lineup and str(game["id"]) in self.lineup["starting_lineup"]:
                        starting_lineup_dict = self.lineup["starting_lineup"][str(game["id"])]
                    for color in ['blue', 'white']:
                        for number, player in sorted(team_players[color].items(), key=lambda t: int(t[0])):
                            name = player['name']
                            if team[color] not in lineup_dict:
                                # lineup not set for that team yet
                                continue
                            player_list = lineup_dict[team[color]]["member"]
                            try:
                                status = player_list[number]
                            except KeyError:
                                # Bug, see issue #31
                                # (After adding a player to the roster, lineup is not updated)
                                status = "out"
                            if number == lineup_dict[team[color]]["captain"]:
                                status = 'captain'
                                #XXX: What if by mistake leader was exchanged?
                            else:
                                # Find players that were exchanged IN (except of captain,
                                # who will just be status = captain even if exchanged in)
                                if starting_lineup_dict is not None:
                                    try:
                                        starting_status = starting_lineup_dict[team[color]]["member"][number]
                                        if starting_status == "sub" and status == "water":
                                            status = "exchanged in"  # exchanged out players habe status 'exchanged'
                                    except KeyError as error:
                                        print(f"ERROR in starting_lineup, can't check player '{number}' for 'exchanged in' status. Traceback: {error}")
                            lineup_lines.append(f"{color},{number},{name},{status}")

        lineup_string = '\n'.join(lineup_lines)

        with self.script_lock:
            try:

                print(r"Lineup update. Sending UDP message: " + "\n" + lineup_string)
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                    sock.sendto(bytes(lineup_string, "utf-8"), (args.udp_ip, args.udp_port))

            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

    def update_gameday(self):

        # We need to get tournament/roster and tournament/gameday first
        if self.gameday is None or self.roster is None:
            return

        gameday_lines = ["gameday", "game_id,blue_id,white_id,blue_name,white_name,score_blue,score_white,time"]
        for game in self.gameday:
            game_id = game['id']
            blue_id = game['blue']
            blue_name = ""
            # If team is deleted, ID is still present, but name is gone
            if blue_id and game['blue'] in self.roster:
                blue_name = self.roster[game['blue']]['name']
            white_id = game['white']
            white_name = ""
            if white_id and game['white'] in self.roster:
                white_name = self.roster[game['white']]['name']
            plan_mqtt = game['plan']
            # fix time difference 1 hour
            date_str, time_str = plan_mqtt.split("T")
            time_str_wo_Z = time_str[:-1]
            hour_str, min_str, sec_str = time_str_wo_Z.split(":")
            new_hour_str = str(int(hour_str) + 1)
            new_time_str = ":".join([new_hour_str, min_str, sec_str]) + "Z"
            plan = "T".join([date_str, new_time_str])
            if game['score']:
                scores = game['score'].split(':')
                #score_blue = f"{int(scores[0].strip()):02d}"
                #score_white = f"{int(scores[1].strip()):02d}"
                score_blue = scores[0].strip()
                score_white = scores[1].strip()
            else:
                score_blue = ""
                score_white = ""
            gameday_lines.append(
                f"{game_id},{blue_id},{white_id},{blue_name},{white_name},{score_blue},{score_white},{plan}"
            )

        gameday_udp_string = '\n'.join(gameday_lines)

        with self.script_lock:
            try:
                print(r"Gameday update. Sending UDP message: " + "\n" + gameday_udp_string)
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                    sock.sendto(bytes(gameday_udp_string, "utf-8"), (args.udp_ip, args.udp_port))

            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

    def update_events(self):

        # We need to get tournament/roster and tournament/gameday first
        if self.event_list is None or self.gameday is None or self.active_game_id is None or self.roster is None:
            return

        active_game = None
        for game in self.gameday:
            if game['id'] == self.active_game_id:
                active_game = game
                break

        if active_game is None:
            return

        #last_events = self.event_list[-1]
        #gametime = last_events['gametime'].strip()
        event_lines = [
            #f"events,{gametime}",
            f"events",
            "halftime,event_idx,gametime,event_type,teamcolor,player_number,player_name,other_player_nr,other_player_name,comment,team_int_id"
        ]
        for idx in range(1, len(self.event_list) + 1):
            gametime = self.event_list[-idx]['gametime'].strip()
            halftime = self.event_list[-idx]['halftime']
            for event in self.event_list[-idx]['events'][::-1]:
                # remove str once linus has changes types
                player_dict = ""
                player_nr = str(event['player_nr'])
                teamcolor = event['teamcolor']
                player_name = ""
                team_int_id = 0  # not set
                if teamcolor:
                    if teamcolor not in active_game:
                        # team was deleted and no new team set yet
                        continue
                    team_id = active_game[teamcolor]
                    team_int_id = self.team_int_ids[team_id]
                    player_dict = self.roster[active_game[teamcolor]]['players']
                    if player_nr and player_nr in player_dict:
                        # remove int once playerexchange fixed by linus (currently a string)
                        player_name = player_dict[player_nr]['name']
                other_player_nr = ""
                other_player_name = ""
                comment = event['comment']

                if event['event_type'] == "penaltythrow":
                    event_type = "penalty throw"
                    if player_nr:
                        Logger.warn("Penalty throw with player number, should not be?")
                    player_nr = ""
                    player_name = ""
                    # Switch penalty against (German rules) or for (international rules)
                    penalty_is_against = False
                    if penalty_is_against:
                        # Bit confusing. If event is AGAINST a team, teamcolor is the
                        # defending team, but we want to show icon of attacking team, so we
                        # start with other_teamcolor"
                        other_teamcolor = teamcolor
                        teamcolor = "blue" if other_teamcolor == "white" else "white"
                    else:
                        other_teamcolor = "blue" if teamcolor == "white" else "white"
                    other_player_dict = ""
                    split_comment = comment.split(";")
                    original_comment = comment
                    comment = ""
                    for split in split_comment:
                        # split = ' <nr> <attacking/defending> '
                        words = split.strip().split(" ")
                        #if len(words) == 2 and words[0].isnumeric() and words[1] == "verteidigt":
                        if len(words) == 2 and words[0].isnumeric() and words[1] == "defending":
                            if not other_player_nr:
                                other_player_nr = words[0]
                            else:
                                Logger.warn("penalty throw comment has wrong format, skipping")
                                comment = original_comment
                                player_nr = ""
                                other_player_dict = ""
                                continue
                        #elif len(words) == 3 and words[0].isnumeric() and words[1] == "greift" and words[2] == "an":
                        elif len(words) == 2 and words[0].isnumeric() and words[1] == "attacking":
                            if not player_nr:
                                player_nr = words[0]
                            else:
                                Logger.warn("penalty throw comment has wrong format, skipping")
                                comment = original_comment
                                player_nr = ""
                                other_player_dict = ""
                                continue
                        else:
                            comment += split
                    if penalty_is_against:
                        # attacking player
                        if other_teamcolor:
                            # Send executing team information in event
                            team_id = active_game[teamcolor]
                            team_int_id = self.team_int_ids[team_id]
                            if other_teamcolor not in active_game:
                                # team was deleted and no new team set yet
                                continue
                            other_player_dict = self.roster[active_game[teamcolor]]['players']
                            if player_nr and player_nr in other_player_dict:
                                player_name = other_player_dict[player_nr]['name']
                        # defending player
                        if teamcolor:
                            if teamcolor not in active_game:
                                # team was deleted and no new team set yet
                                continue
                            if other_player_nr and other_player_nr in player_dict:
                                other_player_name = player_dict[other_player_nr]['name']
                    else:  # not penalty_is_against
                        # attacking player
                        if teamcolor:
                            if teamcolor not in active_game:
                                # team was deleted and no new team set yet
                                continue
                            # Send executing team information in event
                            team_id = active_game[teamcolor]
                            team_int_id = self.team_int_ids[team_id]
                            if player_nr and player_nr in player_dict:
                                player_name = player_dict[player_nr]['name']
                        # defending player
                        if other_teamcolor:
                            if other_teamcolor not in active_game:
                                # team was deleted and no new team set yet
                                continue
                            other_player_dict = self.roster[active_game[other_teamcolor]]['players']
                            if other_player_nr and other_player_nr in other_player_dict:
                                other_player_name = other_player_dict[other_player_nr]['name']

                elif event['event_type'] == "playerexchange":
                    event_type = "player exchange"
                    other_player_nr = comment.split(" ")[0]
                    other_player_name = ""
                    if teamcolor:
                        if teamcolor not in active_game:
                            # team was deleted and no new team set yet
                            continue
                        if other_player_nr and other_player_nr in player_dict:
                            other_player_name = player_dict[other_player_nr]['name']
                    comment = ""
                elif event['event_type'] == "teamwarning":
                    event_type = "team warning"
                elif event['event_type'] == "playerwarning":
                    event_type = "player warning"
                elif event['event_type'] == "refereeball":
                    event_type = "referee ball"
                elif event['event_type'] == "teamball":
                    event_type = "team ball"
                elif event['event_type'] == "doublepenaltytime":
                    event_type = "double penalty time"
                elif event['event_type'] == "penaltytime":
                    event_type = "penalty time"
                elif event['event_type'] == "matchpenalty":
                    event_type = "match penalty"
                elif event['event_type'] == "freethrow":
                    event_type = "free throw"
                    # Send executing team information in event
                    if teamcolor:
                        other_teamcolor = "blue" if teamcolor == "white" else "white"
                        team_id = active_game[other_teamcolor]
                        team_int_id = self.team_int_ids[team_id]
                elif event['event_type'] == "halftimebreak":
                    event_type = "half time break"
                else:
                    event_type = event['event_type']
                event_lines.append(
                    f"{halftime},{idx},{gametime},{event_type},{teamcolor},{player_nr},{player_name},{other_player_nr},{other_player_name},{comment},{team_int_id}"
                )

        events_udp_string = '\n'.join(event_lines)

        with self.script_lock:
            try:
                print(r"Events update. Sending UDP message: " + "\n" + events_udp_string)
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                    sock.sendto(bytes(events_udp_string, "utf-8"), (args.udp_ip, args.udp_port))

            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

def emulator_thread(smc):
    msg = mqtt.MQTTMessage()
    msg.topic = 'state/teamname_blue'
    msg.payload = 'Rheine'
    smc.on_message(None, None, msg)
    msg = mqtt.MQTTMessage()
    msg.topic = 'state/teamname_white'
    msg.payload = 'Hamburg'
    smc.on_message(None, None, msg)
    count = 0
    halftime = 0
    score_blue = 0
    score_white = 0
    while True:
        countdown = 900 - count
        m, s = (np.floor(divmod(count, 60)))
        up = '%02d:%02d' % (m, s)
        m, s = (np.ceil(divmod(countdown, 60)))
        down = '%02d:%02d' % (m, s)
        msg = mqtt.MQTTMessage()
        msg.topic = 'state/current_time'
        msg.payload = ('{"down": "' + down + '", '
                       '"real": "2017-05-11T00:22:56.058549", '
                       '"up": "' + up + '"}')
        smc.on_message(None, None, msg)
        if (count % 60) == 0:
            halftime = halftime + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/game_state'
            msg.payload = ('{"is_halftime_break": false, '
                           '"start_time": "2017-05-11T00:22:29.811534", '
                           '"is_gametime_running": true, '
                           '"has_started": true, '
                           '"is_timeout": false, '
                           '"end_time": "", '
                           '"game_id": -1, '
                           '"n_halftime": ' + str(halftime) + ', '
                           '"is_penaltythrow": false, '
                           '"is_match_over": false}')
            smc.on_message(None, None, msg)
        if (count % 20) == 0:
            score_blue = score_blue + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/score_blue'
            msg.payload = str(score_blue)
            smc.on_message(None, None, msg)
        if (count % 20) == 10:
            score_white = score_white + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/score_white'
            msg.payload = str(score_white)
            smc.on_message(None, None, msg)
        count = count + 1
        count = count % 900
        time.sleep(1)


if __name__ == '__main__':
    smc = StreamMqttClient()
    with smc:
        if IS_EMULATOR:
            t = threading.Thread(target=emulator_thread,
                                 args=[smc])
            t.daemon = True
            t.start()
        while True:
            try:
                threading.Thread(target=smc.update_game_state).start()
            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

            try:
                threading.Thread(target=smc.update_gameday).start()
            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

            try:
                threading.Thread(target=smc.update_lineup).start()
            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

            try:
                threading.Thread(target=smc.update_events).start()
            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)
            time.sleep(1)

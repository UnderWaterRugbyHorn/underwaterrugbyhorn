#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import paho.mqtt.client as mqtt
import json
import os
import csv
import logging
import hornconfig
if not __name__ == '__main__':
    import hornconfig

Logger = logging.getLogger(__name__)

def send_roster(roster):
    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    client.username_pw_set("GameController", hornconfig.mqtt_password)

    client.connect("localhost", port=8883)
    client.loop_start()
    client.publish('tournament/roster', roster, retain=True)
    Logger.info("Roster sent successfully!")
    client.loop_stop()
    client.disconnect()

def import_roster():
    directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'team_lists')

    if not os.path.exists(directory):
        os.makedirs(directory)

    file_path = os.path.join(directory, "lists.csv")
    
    roster = {}

    team_placeholders = {
        "1": ["Ålesund", "m", "NO"],
        "2": ["Barcelona m", "m", "ES"],
        "3": ["Brisbane Gauls", "m", "AU"],
        "4": ["DUC Krefeld", "m", "DE"],
        "5": ["Ege Üniversitesi", "m", "TR"],
        "6": ["FIRS", "m", "IT"],
        "7": ["Flipper", "m", "DK"],
        "8": ["Hammerheads m", "m", "US"],
        "9": ["Orcas m", "m", "CO"],
        "10": ["SDK Malmö Triton", "m", "SE"],
        "11": ["TJ CZU TRITON", "m", "CZ"],
        "12": ["Urheilusukeltajat", "m", "FI"],
        "13": ["USZ Zürich", "m", "CH"],
        "14": ["UWRC Wien m", "m", "AT"],
        "15": ["Amager UVR", "f", "DK"],
        "16": ["Barcelona f", "f", "ES"],
        "17": ["BSI Boblen", "f", "NO"],
        "18": ["Azzurra TEAM", "f", "IT"],
        "19": ["Helvetia", "f", "CH"],
        "20": ["Isbjörnarna", "f", "SE"],
        "21": ["Hammerheads f", "f", "US"],
        "22": ["Orcas f", "f", "CO"],
        "23": ["TC Stuttgart", "f", "DE"],
        "23": ["UWRC Wien f", "f", "AT"]
    }

    try:
        with open(file_path, encoding='utf-8') as file:
            new_team = True
            empty_line = False
            i = 0
            for line in file:
                if line.strip().strip(",") == "" and empty_line:
                    new_team = True
                    continue
                if line.strip().strip(",") == "":
                    empty_line = True
                    continue
                empty_line = False
                if new_team:
                    i = i + 1
                    firstLine = line.strip().strip(",").split(",")
                    teamname = firstLine[0]
                    gender = firstLine[1]
                    nation = firstLine[2]
                    Logger.info("Writing team: " + teamname)
                    roster[str(i)] = {"name": teamname, "nation": nation, "gender_category": gender,
                                        "players": {}}
                    new_team = False
                    continue
            
                player = line.strip().split(',')
                if len(player) == 2:
                    player.append("")
                roster[str(i)]["players"][player[0].strip()] = {"name": player[1].strip(),
                                                            "passnumber": player[2].strip()}
    except FileNotFoundError:
        Logger.info('No lists found, adding placeholders without players')
        for i in range(len(team_placeholders)):
            roster[str(i+1)] = {"name": team_placeholders[str(i+1)][0], 
                                "nation": team_placeholders[str(i+1)][2], 
                                "gender_category": team_placeholders[str(i+1)][1],
                                "players": {}}

    dummy_teams = {
        "1A": "1. Place Grp. A",
        "2A": "2. Place Grp. A",
        "3A": "3. Place Grp. A",
        "1B": "1. Place Grp. B",
        "2B": "2. Place Grp. B",
        "3B": "3. Place Grp. B",
        "4B": "4. Place Grp. B",
        "1C": "1. Place Grp. C",
        "2C": "2. Place Grp. C",
        "3C": "3. Place Grp. C",
        "4C": "4. Place Grp. C",
        "1D": "1. Place Grp. D",
        "2D": "2. Place Grp. D",
        "3D": "3. Place Grp. D",
        "E1": "Winner Game 29",
        "E2": "Winner Game 30",
        "E3": "Loser Game 29",
        "E4": "Loser Game 30",
        "F1": "Winner Game 33",
        "F2": "Winner Game 34",
        "F3": "Loser Game 33",
        "F4": "Loser Game 34",
        "G1": "Winner Game 37",
        "G2": "Winner Game 38",
        "G3": "Loser Game 37",
        "G4": "Loser Game 38",
        "H1": "Winner Game 49",
        "H2": "Winner Game 50",
        "H3": "Loser Game 49",
        "H4": "Loser Game 50",
        "I1": "Winner Game 47",
        "I2": "Winner Game 48",
        "I3": "Loser Game 47",
        "I4": "Loser Game 48",
        "J1": "Winner Game 43",
        "J2": "Winner Game 44",
        "J3": "Loser Game 43",
        "J4": "Loser Game 44",
        "1AF": "1. Place Grp. AF",
        "2AF": "2. Place Grp. AF",
        "3AF": "3. Place Grp. AF",
        "4AF": "4. Place Grp. AF",
        "5AF": "5. Place Grp. AF",
        "1BF": "1. Place Grp. BF",
        "2BF": "2. Place Grp. BF",
        "3BF": "3. Place Grp. BF",
        "4BF": "4. Place Grp. BF",
        "5BF": "5. Place Grp. BF",
        "C1": "Winner Game 51",
        "C2": "Winner Game 52",
        "C3": "Loser Game 51",
        "C4": "Loser Game 52",
        "D1": "Winner Game 53",
        "D2": "Winner Game 54",
        "D3": "Loser Game 53",
        "D4": "Loser Game 54",
        "ref1": ["Birgit Lüdke", "DE"],
        "ref2": ["Manuel Tito de Morais", "SE"],
        "ref3": ["Robert Glock", "AT"],
        "ref4": ["Bob Robinson", "DE"],
        "ref5": ["Serkan Başgül", "TR"],
        "ref6": ["Juliana Colorado", "CO"],
        "ref7": ["Jyrki Mutta", "FI"],
        "ref8": ["Cesar Florez", "CD"],
        "ref9": ["Christoph Pohl", "DE"],
        "ref10": ["Kai Throndsen", "NO"],
        "ref11": ["Esteban Zapata Rojas", "CO"],
        "ref12": ["Kajsa Lindman", "SE"],
        "ref13": ["Julia Braunegg", "AT"],
        "ref14": ["Peter Sögaard", "SE"],
    }

    Logger.info("Adding dummy teams")
    for name_id, name in dummy_teams.items():
        if type(name) == list:
            roster[name_id] = {"name": name[0], "nation": name[1], "gender_category": "", "players": {}}
        else:
            roster[name_id] = {"name": name, "nation": "", "gender_category": "", "players": {}}

    return roster


if __name__ == '__main__':
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    log_formatter = logging.Formatter("[%(levelname)s][%(name)s]"
                                      "[%(module)s][%(funcName)s]"
                                      "[%(threadName)s]%(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    roster = import_roster()
    send_roster(json.dumps(roster))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import horntimers

Logger = logging.getLogger(__name__)

class GameStop():

    new_event_id = 0
    
    def __init__(self, id, game, halftime, gametime, gametime_text):
        self.id = id
        self.game = game
        self.halftime = halftime
        self.gametime = gametime
        self.gametime_text = gametime_text
        self.events = []

    def add_event(self, time_length=None, **kwargs):
        new_id = GameStop.new_event_id
        GameStop.new_event_id += 1
        if time_length is not None:
            kwargs['timer_id'] = self.game.add_linked_timer(time_length,
                                                            self.gametime,
                                                            teamcolor=kwargs['teamcolor'],
                                                            halftime=self.halftime)
        self.events.append(ProtocolEvent(id=new_id, **kwargs))

    def delete_event(self, event_id):
        event = self.get_event(event_id)
        if event is not None:
            if event.timer_id is not None:
                self.game.delete_linked_timer(event.timer_id)
            self.events.remove(event)

    def edit_event(self, event_id, time_length=None, **kwargs):
        event = self.get_event(event_id)
        if event is not None:
            if time_length is not None:
                if event.timer_id is not None:
                    self.game.delete_linked_timer(event.timer_id)
                kwargs['timer_id'] = self.game.add_linked_timer(time_length,
                                                                self.gametime,
                                                                teamcolor=kwargs['teamcolor'],
                                                                halftime=self.halftime)
            event.edit(**kwargs)
            if event.timer_id is not None and event.event_type != 'penaltythrow':
                self.game.linked_timers[event.timer_id].teamcolor = event.teamcolor


    def get_event(self, event_id):
        event = None
        for e in self.events:
            if event_id == e.id:
                event = e
                break
        return event

    def adjust_score(self, teamcolor, value):
        for e in self.events:
            if e.event_type == 'goal':
                score = e.extra.split(':')
                score_blue = int(score[0])
                score_white = int(score[1])
                if teamcolor == 'blue':
                    score_blue = score_blue + value
                elif teamcolor == 'white':
                    score_white = score_white + value
                else:
                    raise ValueError('The teamcolor {} is unknown. Use blue or white'
                                     ''.format(teamcolor))
                e.extra = '{} : {}'.format(score_blue, score_white)
                # This assumes that there is only one goal per game stop
                if e.teamcolor == 'blue':
                    return (value, 0)
                elif e.teamcolor == 'white':
                    return (0, value)
                else:
                    raise ValueError('The event {} at the game stop {} is of '
                                     'type "goal" and has an invalid teamcolor "{}".'
                                     ''.format(e.id, self.id, e.teamcolor))
        return (0, 0)

    def has_event_of_type(self, event_type):
        for e in self.events:
            if e.event_type == event_type:
                return True
        return False

    def get_publish(self):
        d = {'id': self.id,
             'gametime': self.gametime_text,
             'halftime': self.halftime,
             'events': [e.get_publish() for e in self.events]}
        return d

    def get_state(self):
        state = {'id': self.id,
                 'gametime': self.gametime,
                 'gametime_text': self.gametime_text,
                 'halftime': self.halftime,
                 'events': [e.get_state() for e in self.events]}
        return state

    def set_state(self, new_state):
        if sorted(self.get_state().keys()) != sorted(new_state.keys()):
            raise ValueError('this new state has not the correct keys')
        self.id = new_state['id']
        self.halftime = new_state['halftime']
        self.gametime = new_state['gametime']
        self.gametime_text = new_state['gametime_text']
        self.events = []
        for e in new_state['events']:
            new_event = ProtocolEvent(None, None)
            new_event.set_state(e)
            self.events.append(new_event)
            GameStop.new_event_id = max(new_event.id + 1, GameStop.new_event_id)


class ProtocolEvent():
    
    def __init__(self, id, event_type,
                 desc='',
                 comment='',
                 extra='',
                 teamcolor='',
                 timer_id=None,
                 player_nr=''):
        self.id = id
        self.desc = desc
        self.comment = comment
        self.extra = extra
        self.timer_id = timer_id
        self.event_type = event_type
        self.player_nr = player_nr
        if teamcolor in ['', 'blue', 'white']:
            self.teamcolor = teamcolor
        else:
            raise ValueError('The teamcolor "{}" is not known. It has '
                             'to be "blue" or "white"'.format(teamcolor))

    def edit(self, **kwargs):
        if 'desc' in kwargs:
            self.desc = kwargs['desc']
        if 'comment' in kwargs:
            self.comment = kwargs['comment']
        if 'extra' in kwargs:
            self.extra = kwargs['extra']
        if 'timer_id' in kwargs:
            self.timer_id =  kwargs['timer_id']
        if 'event_type' in kwargs:
            self.event_type =  kwargs['event_type']
        if 'player_nr' in kwargs:
            self.player_nr =  kwargs['player_nr']
        if 'teamcolor' in kwargs:
            if kwargs['teamcolor'] in ['', 'blue', 'white']:
                self.teamcolor = kwargs['teamcolor']
            else:
                raise ValueError('The teamcolor "{}" is not known. It has '
                                 'to be "blue" or "white"'.format(kwargs['teamcolor']))

    def get_publish(self):
        d = {'id': self.id,
             'desc': self.desc,
             'comment': self.comment,
             'extra': self.extra,
             'timer_id': self.timer_id,
             'player_nr': self.player_nr,
             'teamcolor': self.teamcolor,
             'event_type': self.event_type}
        return d

    def get_state(self):
        d = {'id': self.id,
             'comment': self.comment,
             'extra': self.extra,
             'event_type': self.event_type,
             'player_nr': self.player_nr,
             'teamcolor': self.teamcolor,
             'desc': self.desc,
             'timer_id': self.timer_id}
        return d

    def set_state(self, new_state):
        if sorted(new_state.keys()) != sorted(self.get_state().keys()):
            raise ValueError("The keys of the new state don't match.")
        self.id = new_state['id']
        self.desc = new_state['desc']
        self.comment = new_state['comment']
        self.extra = new_state['extra']
        self.event_type = new_state['event_type']
        self.player_nr = new_state['player_nr']
        self.teamcolor = new_state['teamcolor']
        self.timer_id = new_state['timer_id']

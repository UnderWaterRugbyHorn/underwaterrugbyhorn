SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_FOLDER=$( basename "$SCRIPT_DIR" )
echo "Stopping $SCRIPT_FOLDER..."
sudo systemctl stop "$SCRIPT_FOLDER"
cd docker
echo "Resetting json files..."
rm -fv uwrhorn_gamecontroller/data/tournament_backup.json
rm -fv uwrhorn_gamecontroller/data/current_game.json
rm -fv uwrhorn_gamecontroller/data/current_game_backup.json
git checkout uwrhorn_gamecontroller/data/tournament.json
echo "Updating git..."
git stash
git pull --all
git stash pop
echo "docker-compose down ..."
sudo docker-compose down
echo "docker-compose build --pull ..."
sudo docker-compose build --pull
echo "Starting $SCRIPT_FOLDER..."
sudo systemctl start "$SCRIPT_FOLDER"
cd $SCRIPT_DIR/python_scripts
python import_roster.py
